#!/bin/bash
#
# Print message $3 with log-level $1 and options $2 to STDERR,
# colorized if terminal
# Example:
# for level in INFO DEBUG WARN ERROR; do
#   log $level this is a $level message;
# done;
#
# will output :
# [YYYY-MM-DD HH:MI:SS] INFO: this is INFO message
# [YYYY-MM-DD HH:MI:SS] DEBUG: this is DEBUG message
# [YYYY-MM-DD HH:MI:SS] WARN: this is WARN message
# [YYYY-MM-DD HH:MI:SS] ERROR: this is ERROR message
#
# Usage: log [options] LEVEL
# Options are:
#  -n <source>      - do not output the trailing newline.
#  -p <filepath>    - do not output the prefix data
# Levels are:
# - INFO
# - DEBUG
# - WARN
# - ERROR
# Default prefix is [YYYY-MM-DD HH:MI:SS]

function get_lvl_nb() {
# error levels
DBG=("DEBUG" 0)
INF=("INFO" 1)
WRN=("WARN" 2)
ERR=("ERROR" 3)
LVL_ARR=(
         DBG[@]
         INF[@]
         WRN[@]
         ERR[@]
         )

    local lvl=$1
    # search for corresponding error level number
    for ((i=0; i<${#LVL_ARR[@]}; i++)); do
        if [[ "${lvl}" == "${!LVL_ARR[i]:0:1}" ]]; then
            lvl_nb=${!LVL_ARR[i]:1:1}
        fi;
    done;
    echo ${lvl_nb}
}


log() {
    local newline=
    local code=
    local noprefix=

    OPTIND=1
    while getopts "np" opt; do
        case $opt in
        n)  # newline parameter
            newline="-n"
            ;;
        p)  # don't show the prefix "[YYYY-MM-DD HH:MI:SS] LEVEL"
            noprefix="yes"
            ;;
        esac;
    done;

    # After getopts is done, shift all processed options away with
    shift $((OPTIND-1))
    # now do something with $@

    # global error level setting
    if [[ -z $LOG_LEVEL ]]; then
        LOG_LEVEL=ERROR
    fi;

    local level=${1?}

    shift
    if [[ -z $noprefix ]]; then
        local line="[$(date '+%F %T')] $level: $*";
    else
        local line="$*";
    fi;
    if [[ -t 2 ]]; then
        case "$level" in
            DEBUG)
                code=30
                ;;
            INFO)
                code=36
                ;;
            WARN)
                code=33
                ;;
            ERROR)
                code=31
                ;;
            *)
                code=37
                ;;
        esac;
        if [[ $(get_lvl_nb $level) -ge $(get_lvl_nb $LOG_LEVEL) ]]; then
            echo $newline -e "\e[${code}m${line}\e[0m"
        fi;
    else
        if [[ $(get_lvl_nb $level) -ge $(get_lvl_nb $LOG_LEVEL) ]]; then
            echo $newline "$line"
        fi;
    fi >&2
}
